function [v1,vmem1,phi1,phimem1]=P3DBL_odestep_mem(time,v,vmem,phi,phimem)
global odemem;
if odemem==1
    [v1,vmem1,phi1,phimem1]=P3DBL_ab1(time,v,vmem,phi,phimem);
elseif odemem==2
    [v1,vmem1,phi1,phimem1]=P3DBL_ab2(time,v,vmem,phi,phimem);
elseif odemem==3
    [v1,vmem1,phi1,phimem1]=P3DBL_ab3(time,v,vmem,phi,phimem);
elseif odemem==4
    [v1,vmem1,phi1,phimem1]=P3DBL_ab4(time,v,vmem,phi,phimem);
elseif odemem==5
    [v1,vmem1,phi1,phimem1]=P3DBL_ab5(time,v,vmem,phi,phimem);
elseif odemem==6   
    [v1,vmem1,phi1,phimem1]=P3DBL_ab6(time,v,vmem,phi,phimem);
elseif odemem==16
    [v1,vmem1,phi1,phimem1]=P3DBL_abm6(time,v,vmem,phi,phimem);
else
    fprintf('no mem method is available: check odemem \n');
end