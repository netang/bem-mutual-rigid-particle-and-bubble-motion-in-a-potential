function [curv,nor]=P3DBL_get_surface_curvature(v,normals)
global AllNeighbors NeiBookmarks;
Nvert=length(v); curv=zeros(Nvert,1);

v1=v(AllNeighbors(NeiBookmarks(1:Nvert)),:)-v;
nor=normals;
iz=nor; ix=cross(nor,v1); normix=1./sqrt(dot(ix',ix'))';
ix(:,1)=ix(:,1).*normix; ix(:,2)=ix(:,2).*normix; ix(:,3)=ix(:,3).*normix;
iy=cross(iz,ix);
N5=Nvert*5; N25=Nvert*25; nn=(1:Nvert)';
[ii,jj]=ndgrid(1:5,1:5);
[ib,kb]=ndgrid(ii(:),nn);
indi=ib(:)+5*(kb(:)-1);
[jb,kb]=ndgrid(jj(:),nn);
indj=jb(:)+5*(kb(:)-1);
rhs=zeros(N5,1);
val=zeros(N25,1);
for i=1:5
    vs=v(AllNeighbors(NeiBookmarks(nn)+i-1),:)-v;
    x=dot(vs',ix')'; y=dot(vs',iy')'; z=dot(vs',iz')';
    rhs(i-5+5*nn)=z;
    val(i-25+25*nn)=x; val(i-20+25*nn)=y; val(i-15+25*nn)=x.*x;
    val(i-10+25*nn)=x.*y; val(i-5+25*nn)=y.*y;
end;
Mx=sparse(indi,indj,val,N5,N5);
rhs=Mx\rhs;

curv=-(rhs(-2+5*nn)+rhs(5*nn));
