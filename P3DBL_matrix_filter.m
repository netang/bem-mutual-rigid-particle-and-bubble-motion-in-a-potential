function [Filter]=P3DBL_matrix_filter(v)
global r0
v=v/r0;
N=length(v);
p = 3;
% p=floor(sqrt(N/2));

[PHI,THETA,R] = cart2sph(v(:,1),v(:,2),v(:,3));
THETA=pi/2-THETA;

P=p^2;
M=zeros(N,P);
MT=zeros(P,N);
Filter=zeros(N,N);

for n=0:p-1
    for m=-n:n
        j=(n+1)*(n+1)-n+m;
        Y=zeros(n+1,1);
        for i=1:N 
            Y=legendre(n,cos(THETA(i)));
            if(m>=0)
                M(i,j)=M(i,j)+Y(m+1)*cos(m*PHI(i));
            else
                M(i,j)=M(i,j)+Y(-m+1)*sin(m*PHI(i));
            end; 
       end;    
    end;
end;

MT=transpose(M);
invMTM=max(max(inv(MT*M)));
MM=inv(MT*M)*MT;
Filter=M*MM;