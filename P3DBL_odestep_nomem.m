function [v1,vp,phi1,phip,Rp1,Rpp,Up1,Upp]=P3DBL_odestep_nomem(time, rb, phib, Rp, Up)
global odenomem;
if odenomem==1
    [v1,vp,phi1,phip]=P3DBL_rk1(time,rb,phib);
elseif odenomem==2
    [v1,vp,phi1,phip]=P3DBL_rk2(time,rb,phib);
elseif odenomem==3
    [v1,vp,phi1,phip]=P3DBL_rk3(time,rb,phib);
elseif odenomem==4
    %[v1,vp,phi1,phip]=P3DBL_rk4(time,v,phi);
    [v1,vp,phi1,phip,Rp1,Rpp,Up1,Upp] = rk4(time,rb,phib,Rp,Up);
elseif odenomem==5
    [v1,vp,phi1,phip]=P3DBL_rk5(time,rb,phib);
elseif odenomem==6   
    [v1,vp,phi1,phip]=P3DBL_rk6(time,rb,phib);
else
    fprintf('no nomem method is available: check odenomem \n');
end
