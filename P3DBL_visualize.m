%script gps_user_visualize

mhandles = guihandles(mhx);
set(mhandles.time,'String',num2str(time));

vp=zeros(size(vp0));
vp(:,1)= vp0(:,1) - Rp0(1) + Rp(1);
vp(:,2)= vp0(:,2) - Rp0(2) + Rp(2);
vp(:,3)= vp0(:,3) - Rp0(3) + Rp(3);
% vp(:,1)= vp0(:,1) + Rp0(1) - Rp(1);
% vp(:,2)= vp0(:,2) + Rp0(2) - Rp(2);
% vp(:,3)= vp0(:,3) + Rp0(3) - Rp(3);

Nb = length(v);
Np = length(vp);

vv = zeros(Nb + Np, 3);
vv = [v; vp]; % ��������

text1='Bubble deformation';


mhs=trimesh(facesall,vv(:,1),vv(:,2),vv(:,3),'Facecolor','red','EdgeColor','black','LineWidth', 1); lighting phong;
view([0 0]);
title(text1); 
axis equal;

c=6;
xmin=-c*r0; xmax=2*c*r0;
ymin=-c*r0; ymax=c*r0;
zmin=-c*r0; zmax=c*r0;

xlim([xmin xmax]); 
ylim([ymin ymax]);
zlim([zmin zmax]);

xlabel('x'); ylabel('y'); zlabel('z');

[normals,areas]=P3DBL_get_normals_areas(v);
[Vg]=P3DBL_get_volume(v,normals,areas);
% [Bc]=P3DBL_get_centers(v,areas)
[Bc]=P3DBL_get_centers_mean(v);
%[Vg]=P3DBL_get_volume_all(v);

Vg_mas(hh,:)=Vg./Vg0;
Rp_mas(hh,:)=Rp;
Up_mas(hh,:)=Up;
Bc_mas(hh,:)=Bc;
time_mas(hh)=time/tmax;
hh=hh+1;

drawnow;
