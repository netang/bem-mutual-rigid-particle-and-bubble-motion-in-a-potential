% Показывает графики взаимодействия пузырька с частицей для d = 4, 5, 6, 7,
% 8, 9, 10


linespecs = {'m','y','r','b','g','c','k'}; % the Cell structure is used for complex linespecs, e.g.: 'ko--'
r0 = 1e-5;
for i = 4:10
    %['d_' int2str(i) '_abm6.mat']
    s(i-3).time_mas = load( ['d_' int2str(i) '_abm6.mat'], 'time_mas' );
    s(i-3).Vg_mas   = load( ['d_' int2str(i) '_abm6.mat'], 'Vg_mas'   );
    s(i-3).Rp_mas   = load( ['d_' int2str(i) '_abm6.mat'], 'Rp_mas'   );
    s(i-3).Bc_mas   = load( ['d_' int2str(i) '_abm6.mat'], 'Bc_mas'   );
end
s(8).time_mas = load('d_4_matlab_freq_4.mat', 'time_mas');
s(8).Vg_mas = load('d_4_matlab_freq_4.mat', 'Vg_mas');
s(8).Rp_mas = load('d_4_matlab_freq_4.mat', 'Rp_mas');
s(8).Bc_mas = load('d_4_matlab_freq_4.mat', 'Bc_mas');

% legend_Vg;
% for i = 4:10
%     legend_Vg = [legend_Vg 'd = ' str2int(i)]
% end
figure;
for i = 4:10
    plot( s(i-3).time_mas.time_mas*5, s(i-3).Vg_mas.Vg_mas(:,1).^(1/3), linespecs{i-3} );
    hold on;
end
% plot(s(8).time_mas.time_mas*5, s(8).Vg_mas.Vg_mas(:,1).^(1/3), 'Color', [0.1 1 0.1]);
% plot(time, Y(:,1)/r0);
% legend('d = 4 freq 3', 'd = 5 freq 3', 'd = 6 freq 3', 'd = 7 freq 3', 'd = 8 freq 3', 'd = 9 freq 3', 'd = 10 freq 3', 'd = 4 freq 4', 'Rayleigh-Plesset eq')
% xlabel('t/T');
% ylabel('a/a_0')
% hold off;

r0 = 1e-5;
figure;
for i = 4:10
    plot( s(i-3).time_mas.time_mas*5, (s(i-3).Rp_mas.Rp_mas(:,1)-(i*r0))/r0, linespecs{i-3} );
    hold on;
end
plot(s(8).time_mas.time_mas*5, (s(8).Rp_mas.Rp_mas(:,1)-(4*r0))/r0, 'Color', [0.1 1 0.1]);
legend('d = 4', 'd = 5', 'd = 6', 'd = 7', 'd = 8', 'd = 9', 'd = 10', 'd = 4 freq 4')
xlabel('t/T');
ylabel('(r_p-r_p0)/a_p0');
hold off;


figure;
for i = 4:10
    plot( s(i-3).time_mas.time_mas*5, s(i-3).Bc_mas.Bc_mas(:,1)/r0, linespecs{i-3} );
    hold on;
end
plot(s(8).time_mas.time_mas*5, s(8).Bc_mas.Bc_mas(:,1)/r0, 'Color', [0.1 1 0.1]);
legend('d = 4', 'd = 5', 'd = 6', 'd = 7', 'd = 8', 'd = 9', 'd = 10' , 'd = 4 freq 4')
xlabel('t/T');
ylabel('(r_b-r_b0)/a_b0');
hold off;