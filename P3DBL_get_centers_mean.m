function [Bc]=P3DBL_get_centers_mean(v)

global Nbub indv nodeBN
for ib=1:Nbub
    sui=find(indv==ib);
    vb=zeros(nodeBN,3);
    vb=v(sui,:);
    Bc(ib,1)=mean(vb(:,1));
    Bc(ib,2)=mean(vb(:,2));
    Bc(ib,3)=mean(vb(:,3));
end;

% Bc(1)=mean(v(:,1));
% Bc(2)=mean(v(:,2));
% Bc(3)=mean(v(:,3));