function [AllNeighbors,AllNeighborFaces,NeiBookmarks]=P3DBL_SetVertexNeighborFaceDataStructure(faces)
%find all vertex neighbors, store lists and provide bookmarks

Nvert=max(faces(:)); Nfaces=length(faces);

hist=zeros(Nvert,1);
for i=1:Nfaces
	v1=faces(i,1); v2=faces(i,2); v3=faces(i,3);
	hist(v1)=hist(v1)+1; hist(v2)=hist(v2)+1; hist(v3)=hist(v3)+1;
end;

Nneigh=sum(hist);
NeiBookmarks=zeros(Nvert+1,1); NeiBookmarks(1)=1;
for j=1:Nvert
	NeiBookmarks(j+1)=NeiBookmarks(j)+hist(j);
end;  
clear hist;

AllNeighbors=zeros(Nneigh,1); AllNeighborFaces=zeros(Nneigh,1);
indc=zeros(Nvert,1); indf=zeros(Nvert,1);
for i=1:Nfaces
	v1=faces(i,1); v2=faces(i,2); v3=faces(i,3); 
	nei1=[v2,v3]; nei2=[v1,v3]; nei3=[v1,v2];
  %update for vertex 1:
	neigh=AllNeighbors(NeiBookmarks(v1):NeiBookmarks(v1)+indc(v1)-1);
	nelold=numel(neigh); neigh=union(neigh,nei1); nelnew=numel(neigh);
	indc(v1)=indc(v1)+nelnew-nelold;
	AllNeighbors(NeiBookmarks(v1):NeiBookmarks(v1)+nelnew-1)=neigh;
    AllNeighborFaces(NeiBookmarks(v1)+indf(v1))=i;
    indf(v1)=indf(v1)+1;
  %update for vertex 2:
	neigh=AllNeighbors(NeiBookmarks(v2):NeiBookmarks(v2)+indc(v2)-1);
	nelold=numel(neigh); neigh=union(neigh,nei2); nelnew=numel(neigh);
	indc(v2)=indc(v2)+nelnew-nelold;
	AllNeighbors(NeiBookmarks(v2):NeiBookmarks(v2)+nelnew-1)=neigh;
    AllNeighborFaces(NeiBookmarks(v2)+indf(v2))=i;
    indf(v2)=indf(v2)+1;
  %update for vertex 3:
    neigh=AllNeighbors(NeiBookmarks(v3):NeiBookmarks(v3)+indc(v3)-1);
	nelold=numel(neigh); neigh=union(neigh,nei3); nelnew=numel(neigh);
	indc(v3)=indc(v3)+nelnew-nelold;
	AllNeighbors(NeiBookmarks(v3):NeiBookmarks(v3)+nelnew-1)=neigh;
    AllNeighborFaces(NeiBookmarks(v3)+indf(v3))=i;
    indf(v3)=indf(v3)+1;
end;

