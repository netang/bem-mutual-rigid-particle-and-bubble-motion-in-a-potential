function vertices=Rotation(vertices,Nvert)

% v1=vertices(1,:);
% v2=vertices(2,:);
% 
% v1(3)=0;
% v2(3)=0;
% 
% alpha=acos(dot(v1,v2)/(sqrt(dot(v1,v1))*sqrt(dot(v2,v2))));
% alpha=alpha/2;
alpha=pi;
A=zeros(3,3);
A(1,1)=cos(alpha);
A(1,2)=0;
A(1,3)=sin(alpha);
A(2,1)=0;
A(2,2)=1;
A(2,3)=0;
A(3,1)=-sin(alpha);
A(3,3)=cos(alpha);

for n=1:Nvert
    vertices(n,:)=(A*(vertices(n,:))')';
end;    