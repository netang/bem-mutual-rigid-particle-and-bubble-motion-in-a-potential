function [vnew,vp,phinew,phip]=P3DBL_rk3(time,v,phi)
%one step of the Runge-Kutta 4th order with saving of the rhs at t=time
global ht vpn_init;
[vp,phip,vpn]=P3DBL_rhs(time,v,phi);
vnew=v+.5*ht*vp;
phinew=phi+.5*ht*phip;
timenew=time+.5*ht;
[rk2,rk2_,vpn]=P3DBL_rhs(timenew,vnew,phinew);

vnew=v+ht*rk2;
phinew=phi+ht*rk2_;
timenew=time+ht;
[rk3,rk3_,vpn]=P3DBL_rhs(timenew,vnew,phinew);
vnew=v+(ht/6)*(vp+4*rk2+rk3);
phinew=phi+(ht/6)*(phip+4*rk2_+rk3_);
vpn_init=vpn;