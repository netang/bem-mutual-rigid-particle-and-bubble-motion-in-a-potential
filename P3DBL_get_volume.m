function [Vol]=P3DBL_get_volume(v,normals,areas)

global indv Bc nodeBN Nbub
Vol=zeros(Nbub,1);

r=v-Bc(indv,:);
rn=(dot(r',normals'))';
volume=rn.*areas/3;
for bub=1:Nbub
    Vol(bub)=sum(volume(nodeBN*(bub-1)+1:nodeBN*bub));
end;    







