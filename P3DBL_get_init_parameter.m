function [Pg0,phog0,mg0]=P3DBL_get_init_parameter(v)

global P0 a T0 sigma Rg indv

Pg0=P0+2*sigma./a(indv);
phog0=Pg0/(Rg*T0);
mg0=4*3.14*a(indv).*a(indv).*a(indv).*phog0/3;



