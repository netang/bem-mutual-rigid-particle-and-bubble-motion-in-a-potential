function [vp,phip,Rpp,Upp]=P3DBL_rhs_mvproduct_direct(time,v,phi,Rp,Up)

%right-hand side for time evolution
global sigma rho1 g indv alpha nodeBN
global Pg0 Vg0 gamma omega Ap P0 Filter Nbub

for i=1:Nbub
    k=(nodeBN*(i-1)+1:nodeBN*i)';
    phi(k)=Filter*phi(k);
    v(k,1)=Filter*v(k,1);
    v(k,2)=Filter*v(k,2);
    v(k,3)=Filter*v(k,3);
end;

Nv=length(v);
[normals,areas]=P3DBL_get_normals_areas(v);
[A,B]=P3DBL_get_bem_matrices(v,normals,areas);
 
[curv,normals]=P3DBL_get_surface_curvature(v,normals); 

[w]=P3DBL_get_tangential_velocity(v,phi,areas,normals);

[Vg]=P3DBL_get_volume_all(v);

B=-diag(ones(Nv,1))/2+B;
Pa=-Ap*sin(omega*time);
Pinf=P0+Pa;

rhs=B*phi;
vpn=A\rhs;

vp(:,1)=vpn.*normals(:,1)+(1+alpha)*w(:,1);
vp(:,2)=vpn.*normals(:,2)+(1+alpha)*w(:,2);
vp(:,3)=vpn.*normals(:,3)+(1+alpha)*w(:,3);

u2=dot(vp',vp')';
ww=dot(w',vp')';

phip=(u2/2+alpha*ww+(Pinf-(Pg0(indv)).*((Vg0(indv)./Vg(indv)).^gamma)+2*sigma*curv)/rho1-v(:,3)*g);


for i=1:Nbub
    k=(nodeBN*(i-1)+1:nodeBN*i)';
    phip(k)=Filter*phip(k);
    vp(k,1)=Filter*vp(k,1);
    vp(k,2)=Filter*vp(k,2);
    vp(k,3)=Filter*vp(k,3);
end;





