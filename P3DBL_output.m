%script P3DBL_output
if graphics~=0
    P3DBL_visualize;
end;    
if graphics==2
    P3DBL_save_graphics;
end;
if movie~=0
    P3DBL_save_movie;
end;
if getfiles~=0
    P3DBL_save_data;
end;