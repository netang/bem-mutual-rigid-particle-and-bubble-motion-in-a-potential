function [drb, dphib, dRp, dUp] = RHS(time, rb, phib, Rp, Up)
%right-hand side for time evolution
% input:   rb,        phib,        Rp,       Up         (+ time)
% output:  d(rb)/dt   d(phib)/dt   d(Rp)/dt  d(Up)/dt
global comp_u % флаг -- каким образом считать cpu (прямым алгоритмом), gpu, FMM
if comp_u==0
    vpn=[];
    [drb, dphib, dRp, dUp]=P3DBL_rhs_mvproduct_direct(time, rb, phib, Rp, Up);
else
    if comp_u==1
        vpn=[];
        [drb, dphib]=P3DBL_rhs_mvproduct_gpu(time, rb, phib);
    else
        [dphib, dphib, vpn]=P3DBL_rhs_mvproduct_FMM(time, rb, phib);
        %[vp,phip]=P3DBL_rhs_mvproduct_FMM_check(time,v,phi);
    end;    
end;
