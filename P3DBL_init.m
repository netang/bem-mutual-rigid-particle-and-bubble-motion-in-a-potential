%script P3DBL_init
faces = facesb_end0;
v = verticesb_end0;
size(v)
% лучше заменить на rb; size(rb) = [Nbubble 3]
a = a_b0;
Bc = Bc0;
nodeBN = nodeBN0; 
faceBN = faceBN0; 
indv = indvb0; 
Nbub = Nbub_end0;


facesp = facesp_end0;
vp0=verticesp_end0;
ap = a_p0;
nodePN = nodePN0;
facePN = facePN0; indvp = indvp0;
Npart = Npart_end0;
Rp0=Pc0';
Up0=[0; 0; 0];
Rp=Rp0;
Rp = PaticleCenters;
sizeRp = size(Rp)
Up=Up0;
facesall = facesbp_end0;
S=512;


mP = rhoP*(4/3)*pi*ap.^3;

%timing
tmin=0; tmax=tmax0; ht=ht0; ntf=fout; nts=fsave;
nt=round((tmax-tmin)/ht)+1; tmax=ht*(nt-1); 
ts=ht*linspace(0,nt-1,nt);
touts=ts(1:ntf:nt); nout=length(touts)+1;
touts(nout)=touts(nout-1)+ntf*ht;
tsaves=ts(1:nts:nt); nsave=length(tsaves)+1;
tsaves(nsave)=tsaves(nsave-1)+nts*ht;
hh=1;
ht=ht0;
tmax=tmax0;

%mesh constant parameters
[AllNeighbors,AllNeighborFaces,NeiBookmarks]=P3DBL_SetVertexNeighborFaceDataStructure(faces);



[Pg0,phog0,mg0]=P3DBL_get_init_parameter(v);

%[Vg0]=P3DBL_get_volume_all(v);
[normals0,areas0]=P3DBL_get_normals_areas(v);
[Vg0]=P3DBL_get_volume(v,normals0,areas0);
[Filter]=P3DBL_matrix_filter(vF);
[coef_ab_n]=P3DBL_get_coef_ab_n(odemem,type_scheme);

vpn_init=zeros(length(v),1);

phi=zeros(length(v),1);
% phi -> phib

if (comp_u==2)
    [option]=set_option(v);
end;    

