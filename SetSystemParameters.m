%script P3DBL_set
%specify initial conditions and working regime
GlobalVariables;

%physical properties (basic SI units)
myu1 = 0;           %liquid dynamic viscosity
myu2 = 0;           %bubble dynamic viscosity
rho_l = 1e+3;        %liquid density
sigma = 0.073;      %surface tension
g = 9.8;            %gravity constant

P0 = 1e+5;              %initial liquid pressure 
T0 = 293;               %temperature  
Rg = 8.31;              %gas constant
omega = 200*pi*2e+3;    %frequency of acoustic field 
Ap = 0.5*P0;            % amplitude of acoustic field
kappa = 1.4;            %polytropic exponent
alpha = 0;              %parameter of tangential velocity corrector

%time integration
periodNumb = 5;
tmax0 = periodNumb*2*pi/omega;     %time period;   
ht0=1e-8; %1e-8;%1e-9;            %integration timestep
fout=1;              %output frequency (output happens after every frout timesteps)

%data output
graphics=1;     % 0=no graphic displays during the run; 1=standard graphic display of the output frames; 2=graphics will be stored in files;
movie=0;        % 0=no movies produced during the run time; 1=output frames are stored to standard movies (uncompressed .avi files); in this case graphics will be enabled;
getfiles=0;     % 0=no storage of the output results; 1=output results are stored to the files labeled by the output #;
caselabel='_case1'; %provide user case label (string) to mark all output data files for the current run case;

%emergency backup frequency
fsave=1000;          %backup data necessary for continuation every fsave integration timesteps

%type of solvers used
type_scheme=2; % 1 - AB scheme, 2 - ABM scheme
multistep_mem=6;%4;     %length of prehistory in the solver (should be consisten with the type of the solver (e.g. 4 means that 3 previous rhs will be saved, which is consistent with AB4 solver)
odenomem=4;          %solver without prehistory
odemem=6;            %solver with prehistory 
comp_u=0;

%iterative process
iterative=0;        %0=no iterative process; 1=use iterative process;
maxnit=10;          %max number of iterations allowed per time step
toliter=1e-6;       %tolerance for termination of the iterative process

% %continuation flag
% continu=0;   needlessly!

rhoP = 2000;
