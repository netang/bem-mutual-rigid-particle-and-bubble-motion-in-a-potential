


s(1).time_mas = load('d_2rb__rp_equals_0.5rb.mat', 'time_mas');
s(1).Vg_mas = load('d_2rb__rp_equals_0.5rb.mat', 'Vg_mas');
s(1).Rp_mas = load('d_2rb__rp_equals_0.5rb.mat', 'Rp_mas');
s(1).Bc_mas = load('d_2rb__rp_equals_0.5rb.mat', 'Bc_mas');
s(2).time_mas = load('d_2rb__rp_equals_2rb.mat', 'time_mas');
s(2).Vg_mas = load('d_2rb__rp_equals_2rb.mat', 'Vg_mas');
s(2).Rp_mas = load('d_2rb__rp_equals_2rb.mat', 'Rp_mas');
s(2).Bc_mas = load('d_2rb__rp_equals_2rb.mat', 'Bc_mas');
s(3).time_mas = load('d_4rb__rp_equals_0.5rb.mat', 'time_mas');
s(3).Vg_mas = load('d_4rb__rp_equals_0.5rb.mat', 'Vg_mas');
s(3).Rp_mas = load('d_4rb__rp_equals_0.5rb.mat', 'Rp_mas');
s(3).Bc_mas = load('d_4rb__rp_equals_0.5rb.mat', 'Bc_mas');
s(4).time_mas = load('d_4rb__rp_equals_2rb.mat', 'time_mas');
s(4).Vg_mas = load('d_4rb__rp_equals_2rb.mat', 'Vg_mas');
s(4).Rp_mas = load('d_4rb__rp_equals_2rb.mat', 'Rp_mas');
s(4).Bc_mas = load('d_4rb__rp_equals_2rb.mat', 'Bc_mas');

linespecs = {'m','y','r','b','g','c','k'}; % the Cell structure is used for complex linespecs, e.g.: 'ko--'
r0 = 1e-5;

figure;
for i = 1:2
    plot( s(i).time_mas.time_mas*5, s(i).Vg_mas.Vg_mas(:,1).^(1/3), linespecs{i+2} );
    hold on;
end
legend('R_b = 1^{-5}m, R_p = 0.5Rb (d = 2R_{b0})', 'R_b = 1^{-5}m, R_p = 2Rb (d = 2R_{b0})');
% растояние между стенками пузырька и частицы = 2rb, радиус частицы равен 0.5rb и 2rb; 
hold off;

figure;
for i = 3:4
    plot( s(i).time_mas.time_mas*5, s(i).Vg_mas.Vg_mas(:,1).^(1/3), linespecs{i} );
    hold on;
end
legend('R_b = 1^{-5}m, R_p = 0.5Rb (d = 4R_{b0})', 'R_b = 1^{-5}m, R_p = 2Rb (d = 4R_{b0})');
% растояние между стенками пузырька и частицы = 4rb, радиус частицы равен 0.5rb и 2rb; 
hold off;


% 
% for i = 4:10
%     plot( s(i-3).time_mas.time_mas*5, s(i-3).Vg_mas.Vg_mas(:,1).^(1/3), linespecs{i-3} );
%     hold on;
% end
% plot(s(8).time_mas.time_mas*5, s(8).Vg_mas.Vg_mas(:,1).^(1/3), 'Color', [0.1 1 0.1]);
% plot(time, Y(:,1)/r0);
% legend('d = 4 freq 3', 'd = 5 freq 3', 'd = 6 freq 3', 'd = 7 freq 3', 'd = 8 freq 3', 'd = 9 freq 3', 'd = 10 freq 3', 'd = 4 freq 4', 'Rayleigh-Plesset eq')
% xlabel('t/T');
% ylabel('a/a_0')
% hold off;
% 
% r0 = 1e-5;
% figure;
% for i = 4:10
%     plot( s(i-3).time_mas.time_mas*5, (s(i-3).Rp_mas.Rp_mas(:,1)-(i*r0))/r0, linespecs{i-3} );
%     hold on;
% end
% plot(s(8).time_mas.time_mas*5, (s(8).Rp_mas.Rp_mas(:,1)-(4*r0))/r0, 'Color', [0.1 1 0.1]);
% legend('d = 4', 'd = 5', 'd = 6', 'd = 7', 'd = 8', 'd = 9', 'd = 10', 'd = 4 freq 4')
% xlabel('t/T');
% ylabel('(r_p-r_p0)/a_p0');
% hold off;
% 
% 
% figure;
% for i = 4:10
%     plot( s(i-3).time_mas.time_mas*5, s(i-3).Bc_mas.Bc_mas(:,1)/r0, linespecs{i-3} );
%     hold on;
% end
% plot(s(8).time_mas.time_mas*5, s(8).Bc_mas.Bc_mas(:,1)/r0, 'Color', [0.1 1 0.1]);
% legend('d = 4', 'd = 5', 'd = 6', 'd = 7', 'd = 8', 'd = 9', 'd = 10' , 'd = 4 freq 4')
% xlabel('t/T');
% ylabel('(r_b-r_b0)/a_b0');
% hold off;