%script P3DBL_run
%solves dynamic problem of motion of bubbles in liquid 1 
%(Navier-Stokes equations without viscosity term, Boundary element method for potential flow)
%BEM Group, Computational Lab, Center for Micro and Nanoscale Dynamics of Dispersed Systems, Bashkir State University, Ufa, Russia
%developers: Yu.A. Itkulova, O.A. Abramova, N.A. Gumerov

if continu == 0              %initialization from scratch
    
%     copyfile('P3DBL_set.m',['P3DBL_set' caselabel '.m']);   %copy of settings for the current case is be created
    P3DBL_init;
    
    if movie~=0
        if graphics==0
            graphics=2;
        end;
        mov=avifile(['P3DBL' caselabel '.avi'],'compression','None');
    end;
    if graphics~=0 
        P3DBL_set_graphics;
    end;
    
    it=1; time=0; 
    
    iout=1;                 %output initial state
    P3DBL_output;
    iout=iout+1; 
    
    isave=1;
    
    if multistep_mem > 1
        sizev=size(v); lszv=length(sizev); sizev(lszv+1)=multistep_mem-1;
        vmem=zeros(sizev);
        phimem=zeros(length(v),multistep_mem-1);
        Rpmem=zeros(3,multistep_mem-1);
        Upmem=zeros(3,multistep_mem-1);
    end;
           
end;

tcpu0=cputime;
it0=it;

%main time loop
while it<nt
   
    tcpu=cputime;
   
    %solver
    if multistep_mem > 1
        if it<multistep_mem
            [v,vp,phi,phip,Rp,Rpp,Up,Upp]=P3DBL_odestep_nomem(time,v,phi,Rp,Up);
             vab = v;
             vpab = vp;
             phiab = phi;
            phipab =phip;
            Rpab = Rp;
            Rppab = Rpp;
            Upab = Up;
            Uppab = Upp;
            mempos=multistep_mem-it;
            vmem(:,:,mempos)=vp;
            phimem(:,mempos)=phip;
            Rpmem(:,mempos)=Rpp;
            Upmem(:,mempos)=Upp;
            vmemab = vmem;
            phimemab = phimem;
            Rpmemab = Rpmem;
            Upmemab = Upmem;
        else
            %[v,vmem,phi,phimem]=P3DBL_odestep_mem(time,v,vmem,phi,phimem);
            if type_scheme==1
                [v,vmem,phi,phimem,Rp,Rpmem,Up,Upmem]=P3DBL_ab_n(time,v,vmem,phi,phimem,Rp,Rpmem,Up,Upmem);  
            else
%                 [v,vmem,phi,phimem,Rp,Rpmem,Up,Upmem]=P3DBL_abm_n(time,v,vmem,phi,phimem,Rp,Rpmem,Up,Upmem);  %ABM scheme
%                 [v4,vmem4,phi4,phimem4,Rp4,Rpmem4,Up4,Upmem4]=P3DBL_ab4(time,v,vmem,phi,phimem,Rp,Rpmem,Up,Upmem); 
                  [v,vmem,phi,phimem,Rp,Rpmem,Up,Upmem]=P3DBL_abm6(time,v,vmem,phi,phimem,Rp,Rpmem,Up,Upmem); 
%                 [v6,vmem6,phi6,phimem6,Rp6,Rpmem6,Up6,Upmem6]
                %[vab,vmemab,phiab,phimemab,Rpab,Rpmemab,Upab,Upmemab]=P3DBL_ab_n(time,vab,vmemab,phiab,phimemab,Rpab,Rpmemab,Upab,Upmemab); 
%                        max(abs(v - vab))
%                        max(abs(Rpab -Rp))
%                 [v,vmem,phi,phimem,Rp,Rpmem,Up,Upmem] - [v6,vmem6,phi6,phimem6,Rp6,Rpmem6,Up6,Upmem6]
            end;
            
        end;
    else
        [v,vp,phi,phip,Rp,Rpp,Up,Upp]=P3DBL_odestep_nomem(time,v,phi,Rp,Up);
    end;
    
    it=it+1; 
    time=(it-1)*ht;
    
    %output
    if abs(time-touts(iout))<0.1*ht
       iout=iout+1;
       P3DBL_output;
    end;
    
    if abs(time-tsaves(isave))<0.1*ht
       isave=isave+1;
       P3DBL_savestate;
    end;
         
    steptime=cputime-tcpu;
    passedtime=cputime-tcpu0;
    lefttime=passedtime*(nt-it)/(it-it0);
    step = time/ht0
    
    fprintf('step= %f, time= %f,  cputime step= %f, cputime passed= %f, cputime left= %f\n',step, time,steptime,passedtime,lefttime);        

    
end;

if movie ~=0
    mov=close(mov);
end;

fprintf('\n'); fprintf('S3D2F_run finished \n');
fprintf('to continue specify new tmax and run S3D2F_continue \n');
