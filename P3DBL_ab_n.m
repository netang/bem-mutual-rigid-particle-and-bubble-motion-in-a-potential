function [vnew,vmemnew,phinew,phimemnew,Rpnew,Rpmemnew,Upnew,Upmemnew]=P3DBL_ab_n(time,v,vmem,phi,phimem,Rp,Rpmem,Up,Upmem)
%one step of the Adams-Bashforth 6th order 
global ht vpn_init coef_ab_n odemem; 
[vp,phip,Rpp,Upp]=P3DBL_rhs(time,v,phi,Rp,Up);
vmemnew=zeros(size(vmem));
phimemnew=zeros(size(phimem));
Rpmemnew=zeros(size(Rpmem));
Upmemnew=zeros(size(Upmem));

vnew=coef_ab_n(1)*vp;
phinew=coef_ab_n(1)*phip;
Rpnew=coef_ab_n(1)*Rpp;
Upnew=coef_ab_n(1)*Upp;

for i=1:odemem-1
   vnew=vnew+coef_ab_n(i+1)*vmem(:,:,i); 
   phinew=phinew+coef_ab_n(i+1)*phimem(:,i); 
   Rpnew=Rpnew+coef_ab_n(i+1)*Rpmem(:,i); 
   Upnew=Upnew+coef_ab_n(i+1)*Upmem(:,i); 
end;
vnew=v+ht*vnew;
phinew=phi+ht*phinew;
Rpnew = Rp + ht*Rpnew;
Upnew = Up + ht*Upnew;

for i=odemem-2:-1:1
   vmemnew(:,:,i+1)=vmem(:,:,i);
   phimemnew(:,i+1)=phimem(:,i);
   Rpmemnew(:,i+1)=Rpmem(:,i);
   Upmemnew(:,i+1)=Upmem(:,i);
end;
vmemnew(:,:,1)=vp;
phimemnew(:,1)=phip;
Rpmemnew(:,1)=Rpp;
Upmemnew(:,1)=Upp;

% vpn_init=vpn;

% vnew=v+ht/1440*(4277*vp-7923*vmem(:,:,1)+9982*vmem(:,:,2)-7298*vmem(:,:,3)+2877*vmem(:,:,4)-475*vmem(:,:,5));
% phinew=phi+ht/1440*(4277*phip-7923*phimem(:,1)+9982*phimem(:,2)-7298*phimem(:,3)+2877*phimem(:,4)-475*phimem(:,5));
% vmemnew(:,:,5)=vmem(:,:,4); vmemnew(:,:,4)=vmem(:,:,3); vmemnew(:,:,3)=vmem(:,:,2); vmemnew(:,:,2)=vmem(:,:,1); vmemnew(:,:,1)=vp;
% phimemnew(:,5)=phimem(:,4); phimemnew(:,4)=phimem(:,3); phimemnew(:,3)=phimem(:,2); phimemnew(:,2)=phimem(:,1); phimemnew(:,1)=phip;
% vpn_init=vpn;

