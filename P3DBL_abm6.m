%function [vnew,vmemnew,phinew,phimemnew]=P3DBL_abm6(time,v,vmem,phi,phimem)
function [vnew,vmemnew,phinew,phimemnew,Rpnew,Rpmemnew,Upnew,Upmemnew]=P3DBL_abm6(time,v,vmem,phi,phimem,Rp,Rpmem,Up,Upmem)
%one step of the Adams-Bashforth-Moulton 6th order predictor-corrector 
global ht;    
%[vp,phip]=P3DBL_rhs(time,v,phi);
[vp,phip,Rpp,Upp] = RHS(time,v,phi,Rp,Up);
vmemnew=zeros(size(vmem));
phimemnew=zeros(size(phimem));
Rpmemnew = zeros(size(Rpmem));
Upmemnew = zeros(size(Upmem));
vnew=v+ht/1440*(4277*vp-7923*vmem(:,:,1)+9982*vmem(:,:,2)-7298*vmem(:,:,3)+2877*vmem(:,:,4)-475*vmem(:,:,5));
phinew=phi+ht/1440*(4277*phip-7923*phimem(:,1)+9982*phimem(:,2)-7298*phimem(:,3)+2877*phimem(:,4)-475*phimem(:,5));
Rpnew=Rp+ht/1440*(4277*Rpp-7923*Rpmem(:,1)+9982*Rpmem(:,2)-7298*Rpmem(:,3)+2877*Rpmem(:,4)-475*Rpmem(:,5));
Upnew=Up+ht/1440*(4277*Upp-7923*Upmem(:,1)+9982*Upmem(:,2)-7298*Upmem(:,3)+2877*Upmem(:,4)-475*Upmem(:,5));
vmemnew(:,:,5)=vmem(:,:,4); vmemnew(:,:,4)=vmem(:,:,3); vmemnew(:,:,3)=vmem(:,:,2); vmemnew(:,:,2)=vmem(:,:,1); vmemnew(:,:,1)=vp;
phimemnew(:,5)=phimem(:,4); phimemnew(:,4)=phimem(:,3); phimemnew(:,3)=phimem(:,2); phimemnew(:,2)=phimem(:,1); phimemnew(:,1)=phip;
Rpmemnew(:,5) =Rpmem(:,4);  Rpmemnew(:,4) =Rpmem(:,3);  Rpmemnew(:,3) =Rpmem(:,2);  Rpmemnew(:,2) =Rpmem(:,1); Rpmemnew(:,1) =Rpp;
Upmemnew(:,5) =Upmem(:,4); Upmemnew(:,4) =Upmem(:,3);  Upmemnew(:,3) =Upmem(:,2);  Upmemnew(:,2) =Upmem(:,1); Upmemnew(:,1) =Upp;
[vnew,phinew, Rpnew, Upnew]=P3DBL_rhs(time,vnew,phinew, Rpnew, Upnew);
vnew=v+ht/1440*(475*vnew+1427*vp-798*vmem(:,:,1)+482*vmem(:,:,2)-173*vmem(:,:,3)+27*vmem(:,:,4));
phinew=phi+ht/1440*(475*phinew+1427*phip-798*phimem(:,1)+482*phimem(:,2)-173*phimem(:,3)+27*phimem(:,4));
Rpnew=Rp+ht/1440*(475*Rpnew+1427*Rpp-798*Rpmem(:,1)+482*Rpmem(:,2)-173*Rpmem(:,3)+27*Rpmem(:,4) );
Upnew=Up+ht/1440*(475*Upnew+1427*Upp-798*Upmem(:,1)+482*Upmem(:,2)-173*Upmem(:,3)+27*Upmem(:,4) );