function [ f, v, markFaces, markVertices] = BubbleParticleMesh( Bc, Pc, Rb, Rp, frec )
%  BubbleParticleMesh is function for generate Bubbles and Particles mesh
%   Bc is array of bubbles centers
%   Pc is array of particle centers
%   Rb is array of Radius i-th bubbles
%   Rp us array of Radius i-th particles
%   frec is frequency of mesh discretisation
%   Run Example: BubbleParticleMesh( [0 0 0; 2 0 0], [0 4 0; 2 4 0], [0.7 0.5], [0.6 0.5] );

[facesb, verticesb] = SphereMeshZ(frec,1);

v = [];
f = [];
markFaces = [];
markVertices = [];
for i=1:size(Bc, 1)
    f = [f; facesb + size(v, 1)];
    v = [v; [Rb(i)*verticesb(:,1) + Bc(i,1) ...
        Rb(i)*verticesb(:,2) + Bc(i,2) Rb(i)*verticesb(:,2) + Bc(i,2)] ];
    markVertices = [markVertices; i*ones(size(verticesb, 1), 1) ];
    markFaces    = [markFaces;    i*ones(size(facesb,    1), 1) ];
end

for i=1:size(Bc, 1)
    f = [f; facesb + size(v, 1)];
    v = [v; [Rp(i)*verticesb(:,1) + Pc(i,1) ...
        Rp(i)*verticesb(:,2) + Pc(i,2) Rp(i)*verticesb(:,2) + Pc(i,2)] ];
    markVertices = [markVertices; (i+size(Bc, 2))*ones(size(verticesb, 1), 1) ];
    markFaces    = [markFaces;    (i+size(Bc, 2))*ones(size(facesb,    1), 1) ];
end

figure;
trimesh(f, v(:,1), v(:,2), v(:,3));

end

