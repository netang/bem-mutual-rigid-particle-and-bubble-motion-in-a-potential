function [A,B]=P3DBL_get_bem_matrices_gradG(v,normals,areas)
Nvert=length(v);
Nvert3=Nvert*3;
A=zeros(Nvert,Nvert); 
B=zeros(Nvert3,Nvert); 
vmn=zeros(Nvert,3);
for n=1:Nvert
    vn=v(n,:); norn=normals(n,:); arn=areas(n)/(4*pi);
    vmn(:,1)=v(:,1)-vn(1); vmn(:,2)=v(:,2)-vn(2); vmn(:,3)=v(:,3)-vn(3);
    d=sqrt(dot(vmn',vmn')'); d(n)=1; d=1./d; d(n)=0;
    cmn=norn(1)*vmn(:,1)+norn(2)*vmn(:,2)+norn(3)*vmn(:,3); 
    A(:,n)=arn*d;
    dd=d.*d.*d;
    for i=1:3
        B(i:3:Nvert3,n)=arn*vmn(:,i).*dd;
    end;  
end;    

%singular part  B
% for n=1:Nvert
%    B(n,n)=-0.5-sum(B(n,:));
% end; 
% 
% %singular part  A
% C=zeros(3,Nvert);
% E=0.5*eye(Nvert,Nvert);
% C(1,:)=A*normals(:,1)-(B+E)*v(:,1);
% C(2,:)=A*normals(:,2)-(B+E)*v(:,2);
% C(3,:)=A*normals(:,3)-(B+E)*v(:,3);
% for n=1:Nvert
%    A(n,n)=-(normals(n,:))'\C(:,n);
% end;