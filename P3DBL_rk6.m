function [vnew,vp,phinew,phip]=P3DBL_rk6(time,v,phi)
%one step of the Runge-Kutta 4th order with saving of the rhs at t=time
global ht vpn_init;
[vp,phip,vpn]=P3DBL_rhs(time,v,phi);
vnew=v+.5*ht*vp;
phinew=phi+.5*ht*phip;
timenew=time+.5*ht;
[rk2,rk2_,vpn]=P3DBL_rhs(timenew,vnew,phinew);

vnew=v+ht*(2/9*vp+4/9*rk2);
phinew=phi+ht*(2/9*phip+4/9*rk2_);
timenew=time+2/3*ht;
[rk3,rk3_,vpn]=P3DBL_rhs(timenew,vnew,phinew);

vnew=v+ht*(7/36*vp+2/9*rk2-1/12*rk3);
phinew=phi+ht*(7/36*phip+2/9*rk2_-1/12*rk3_);
timenew=time+1/3*ht;
[rk4,rk4_,vpn]=P3DBL_rhs(timenew,vnew,phinew);

vnew=v+ht*(-35/144*vp-55/36*rk2+35/48*rk3+15/8*rk4);
phinew=phi+ht*(-35/144*phip-55/36*rk2_+35/48*rk3_+15/8*rk4_);
timenew=time+5/6*ht;
[rk5,rk5_,vpn]=P3DBL_rhs(timenew,vnew,phinew);

vnew=v+ht*(-1/360*vp-11/36*rk2-1/8*rk3+1/2*rk4+1/10*rk5);
phinew=phi+ht*(-1/360*phip-11/36*rk2_-1/8*rk3_+1/2*rk4_+1/10*rk5_);
timenew=time+1/6*ht;
[rk6,rk6_,vpn]=P3DBL_rhs(timenew,vnew,phinew);

vnew=v+ht*(-41/260*vp+22/13*rk2+43/156*rk3-118/39*rk4+32/195*rk5+80/39*rk6);
phinew=phi+ht*(-41/260*phip+22/13*rk2_+43/156*rk3_-118/39*rk4_+32/195*rk5_+80/39*rk6_);
timenew=time+ht;
[rk7,rk7_,vpn]=P3DBL_rhs(timenew,vnew,phinew);

vnew=v+ht*(13/200*vp+11/40*rk3+11/40*rk4+4/25*rk5+4/25*rk6+13/200*rk7);
phinew=phi+ht*(13/200*phip+11/40*rk3_+11/40*rk4_+4/25*rk5_+4/25*rk6_+13/200*rk7_);
vpn_init=vpn;