clear all;
close all;
frec = 3;
[fF,vF] = SphereMeshZ(frec,1); % vF нужна для создани фильтра

global r0
r0 = 1e-5; % начальный радиус пузырьков и частиц.

Nbub = 1;
% [vertices0,faces0,Bc0,nodeBN0,faceBN0,indv0,Nbub0,a0]=BubblesGenerator_scale(Nbub,frec,r0);
[verticesb_end0,facesb_end0,Bc0,nodeBN0,faceBN0,indvb0,Nbub_end0,a_b0, ...
 verticesp_end0,facesp_end0,Pc0,nodePN0,facePN0,indvp0,Npart_end0,a_p0, facesbp_end0 ] ...
 =BubblesParticlesGenerator_scales(1, 1, frec, frec, r0, r0);

Bc = [0 0    0; 7*r0 0    0];
PaticleCenters = [0 4*r0 0; 7*r0 4*r0 0];
BubbleRadius   = [r0  r0];
ParticleRadius = [r0  r0];
[AllFaces, AllVertices, markFaces, markVertices] = BubbleParticleMesh( Bc, PaticleCenters, BubbleRadius, ParticleRadius, frec );
% error(1);


SetSystemParameters;
EvolutionCycle;
