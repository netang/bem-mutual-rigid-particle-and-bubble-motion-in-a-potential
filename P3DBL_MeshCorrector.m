function [vc,phic]=P3DBL_MeshCorrector(v,phi)
global AllNeighbors NeiBookmarks indv_cont;
eps=0.5;
vc=zeros(size(v));
v0=zeros(size(v));
v0t=zeros(size(v));
phic=zeros(size(phi));
Nvert=length(v);

for i=1:Nvert
   vnei=AllNeighbors(NeiBookmarks(i):NeiBookmarks(i+1)-1);
   v0(i,1)=v(i,1)-mean(v(vnei,1));
   v0(i,2)=v(i,2)-mean(v(vnei,2));
   v0(i,3)=v(i,3)-mean(v(vnei,3));
end;  

[normals,areas]=P3DBL_get_normals_areas(v);
[w]=P3DBL_get_tangential_velocity(v,phi,areas,normals);

v0n=(dot(v0',normals'))';
v0t(:,1)=v0(:,1)-v0n.*normals(:,1);
v0t(:,2)=v0(:,2)-v0n.*normals(:,2);
v0t(:,3)=v0(:,3)-v0n.*normals(:,3);

vc(:,1)=v(:,1)-eps*v0t(:,1);
vc(:,2)=v(:,2)-eps*v0t(:,2);
vc(:,3)=v(:,3)-eps*v0t(:,3);

dv=zeros(size(v));
dv(:,1)=vc(:,1)-v(:,1);
dv(:,2)=vc(:,2)-v(:,2);
dv(:,3)=vc(:,3)-v(:,3);
phic=phi+(dot(dv',w'))';

phic(indv_cont)=phi(indv_cont);
vc(indv_cont,:)=v(indv_cont,:);