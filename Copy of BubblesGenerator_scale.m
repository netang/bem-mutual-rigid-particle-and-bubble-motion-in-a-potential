function [vertices_end,faces_end,Bc,nodeBN,faceBN,indv,Nbub_end,a]=BubblesGenerator_scale(Nbub,sq,r0)
c_sph=zeros(Nbub,3);
r_sph=zeros(Nbub,1);
R_sph=zeros(Nbub,1);
BookmarksVertBub=zeros(Nbub+1,1);
BookmarksFaceBub=zeros(Nbub+1,1);
BookmarksVertBub(1)=1;
BookmarksFaceBub(1)=1;
IndBub=ones(Nbub,1);

eps=1.5; 
scale=Nbub;%3%50 for 10000bub;

c_sph(:,1)=rand(Nbub,1).*scale-scale/2;
c_sph(:,2)=rand(Nbub,1).*scale-scale/2;
c_sph(:,3)=rand(Nbub,1).*scale-scale/2;

r_sph=1+rand(Nbub,1)/2;
R_sph=(1+eps)*r_sph;

for n=1:Nbub
    [faces1,vertices1] = SphereMeshZ(sq,1);
    vertices1=r_sph(n)*vertices1;
    vertices1(:,1)=vertices1(:,1)+c_sph(n,1);
    vertices1(:,2)=vertices1(:,2)+c_sph(n,2);
    vertices1(:,3)=vertices1(:,3)+c_sph(n,3);
    
    BookmarksVertBub(n+1)=BookmarksVertBub(n)+length(vertices1);
    vertices(BookmarksVertBub(n):(BookmarksVertBub(n+1)-1),:)=vertices1;
    BookmarksFaceBub(n+1)=BookmarksFaceBub(n)+length(faces1);
    faces(BookmarksFaceBub(n):(BookmarksFaceBub(n+1)-1),:)=faces1+BookmarksVertBub(n)-1;  
    IndVertices(BookmarksVertBub(n):(BookmarksVertBub(n+1)-1))=n;
end  
nodeBN=length(vertices1);
faceBN=length(faces1);
NV=BookmarksVertBub(n+1)-1;
NF=BookmarksFaceBub(n+1)-1;


figure;
trimesh(faces,vertices(:,1),vertices(:,2),vertices(:,3));
hold on;
axis equal;

array_IndBub=1:Nbub;

for n=1:Nbub
    if(IndBub(n)==1)
        vertices2(:,1)=vertices(:,1)-c_sph(n,1);
        vertices2(:,2)=vertices(:,2)-c_sph(n,2);
        vertices2(:,3)=vertices(:,3)-c_sph(n,3);
        r2=dot(vertices2',vertices2')'/(R_sph(n)*R_sph(n));
        sui2=find(r2<=1);
        IndVertices(sui2);
        suiBub=intersect(IndVertices(sui2),array_IndBub);
        IndBub(suiBub)=0;
        IndBub(n)=1;
    end
end    

n_bub=find(IndBub==1);
Nbub_end=length(n_bub);
BookmarksVertBub_end=zeros(Nbub_end+1,1);
BookmarksFaceBub_end=zeros(Nbub+1,1);
BookmarksVertBub_end(1)=1;
BookmarksFaceBub_end(1)=1;
Bc=zeros(Nbub_end,3);
a=zeros(Nbub_end,1);
for n_end=1:Nbub_end
    n=n_bub(n_end);
    BookmarksVertBub_end(n_end+1)=BookmarksVertBub(n+1)-BookmarksVertBub(n)+BookmarksVertBub_end(n_end);
    vertices_end(BookmarksVertBub_end(n_end):(BookmarksVertBub_end(n_end+1)-1),:)=...
        vertices(BookmarksVertBub(n):(BookmarksVertBub(n+1)-1),:);
    BookmarksFaceBub_end(n_end+1)=BookmarksFaceBub_end(n_end)+BookmarksFaceBub(n+1)-BookmarksFaceBub(n);
    faces_end(BookmarksFaceBub_end(n_end):(BookmarksFaceBub_end(n_end+1)-1),:)=...
        faces(BookmarksFaceBub(n):(BookmarksFaceBub(n+1)-1),:)+...
        BookmarksVertBub_end(n_end)-BookmarksVertBub(n);
    r_end(BookmarksVertBub_end(n_end):(BookmarksVertBub_end(n_end+1)-1),1)=r_sph(n);
    a(n_end)=r_sph(n);
    indv(BookmarksVertBub_end(n_end):(BookmarksVertBub_end(n_end+1)-1),1)=n_end;
    Bc(n_end,:)=r0*c_sph(n,:);
end    

vertices_end=r0*vertices_end;
a=r0*a;
figure;
trimesh(faces_end,vertices_end(:,1),vertices_end(:,2),vertices_end(:,3));
hold on;
axis equal;
