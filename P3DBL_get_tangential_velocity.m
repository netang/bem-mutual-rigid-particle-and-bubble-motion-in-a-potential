function [w]=P3DBL_get_tangential_velocity(v,phi,areas,normals)
global faces;

Nvert=length(v);
Nface=length(faces);
w=zeros(Nvert,3);
uf=zeros(Nface,3);
uv=zeros(Nvert,3);
v1=faces(:,1); v2=faces(:,2); v3=faces(:,3);
r1=v(v1,:); r2=v(v2,:); r3=v(v3,:);

uf(:,1)=(phi(v1).*(r2(:,1)-r3(:,1))+phi(v2).*(r3(:,1)-r1(:,1))+phi(v3).*(r1(:,1)-r2(:,1)))/2;
uf(:,2)=(phi(v1).*(r2(:,2)-r3(:,2))+phi(v2).*(r3(:,2)-r1(:,2))+phi(v3).*(r1(:,2)-r2(:,2)))/2;
uf(:,3)=(phi(v1).*(r2(:,3)-r3(:,3))+phi(v2).*(r3(:,3)-r1(:,3))+phi(v3).*(r1(:,3)-r2(:,3)))/2;

for j=1:Nface
    uv(v1(j),:)=uv(v1(j),:)+uf(j,:);
    uv(v2(j),:)=uv(v2(j),:)+uf(j,:);
    uv(v3(j),:)=uv(v3(j),:)+uf(j,:);
end;
uv(:,1)=uv(:,1)./(3*areas);
uv(:,2)=uv(:,2)./(3*areas);
uv(:,3)=uv(:,3)./(3*areas);

w=cross(uv,normals);
