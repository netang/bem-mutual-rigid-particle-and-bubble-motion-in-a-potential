function [vbp, phibp, Rpp, Upp]=P3DBL_rhs_mvproduct_direct(time, rb, phib, Rp, Up) % 
size(rb)
size(phib)
size(Rp)
size(Up)
%right-hand side for time evolution
% input parameters:   r_b,  phi_b,  R_p,  U_p;
% ouput parameters:  dr_b, dphi_b, dR_p, dU_p;
% global parameters: g, rho_l, rho_p[], kappa, sigma, k
global sigma rho_l g indv alpha nodeBN vp0 Rp0 mP rhoP
global Pg0 Vg0 kappa omega Ap P0 Filter Nbub



for i=1:Nbub
    k=(nodeBN*(i-1)+1:nodeBN*i)';
    phib(k)=Filter*phib(k);
    rb(k,1)=Filter*rb(k,1);
    rb(k,2)=Filter*rb(k,2);
    rb(k,3)=Filter*rb(k,3);
end;

size(Rp0)
size(Rp)
size(vp0)
vp=zeros(size(vp0));
% rewrite vp0(:,1) - Rp0(1) because vp0(:,1) - Rp0(1) - this is static
vp(:,1)= vp0(:,1) - Rp0(1) + Rp(1);
vp(:,2)= vp0(:,2) - Rp0(2) + Rp(2);
vp(:,3)= vp0(:,3) - Rp0(3) + Rp(3);
% must be
% rp = rp_local_vert + Rp;
% 




Nb = length(rb);
Np = length(vp);

v = zeros(Nb + Np, 3);
v = [rb; vp]; % ��������


Nv=length(v);
[normalsb,areasb]=P3DBL_get_normals_areas(rb);
[normalsp,areasp]=P3DBL_get_normals_areas(vp);
%normalsp = -normalsp;
normals = [normalsb; normalsp];
areas   = [areasb;   areasp  ];


[A,B]=P3DBL_get_bem_matrices(v,normals,areas);


[curvb,normalsb]=P3DBL_get_surface_curvature(rb,normalsb); 

[w]=P3DBL_get_tangential_velocity(rb,phib,areasb,normalsb);



[Vg]=P3DBL_get_volume_all(rb);

BB=-diag(ones(Nv,1))/2+B;
Pa=-Ap*sin(omega*time);
Pinf=P0+Pa;

nor_ar_p(:,1)=normalsp(:,1).*areasp;
nor_ar_p(:,2)=normalsp(:,2).*areasp;
nor_ar_p(:,3)=normalsp(:,3).*areasp;


rhs=BB(:,1:Nb)*phib;
NNp = A(:,Nb+1:end)*normalsp;
HHp = NNp*Up;

BBp = rho_l/mP*NNp*nor_ar_p';

AA=[A(:,1:Nb) -B(:,Nb+1:end)+BBp];


x=AA\(rhs - HHp);

vpn=x(1:Nb);
phip = x(Nb+1:end);
[wp] = P3DBL_get_tangential_velocity(vp,phip,areasp,normalsp);


vbp(:,1)=vpn.*normalsb(:,1)+(1+alpha)*w(:,1);
vbp(:,2)=vpn.*normalsb(:,2)+(1+alpha)*w(:,2);
vbp(:,3)=vpn.*normalsb(:,3)+(1+alpha)*w(:,3);

u2=dot(vbp',vbp')';
ww=dot(w',vbp')';



phibp=(u2/2+alpha*ww+(Pinf-(Pg0(indv)).*((Vg0(indv)./Vg(indv)).^kappa)+2*sigma*curvb)/rho_l-rb(:,3)*g);

coef_ar=1;

nor_ar_pp=zeros(size(normalsp));
nor_ar_pp(:,1)=normalsp(:,1).*areasp*coef_ar;
nor_ar_pp(:,2)=normalsp(:,2).*areasp*coef_ar;
nor_ar_pp(:,3)=normalsp(:,3).*areasp*coef_ar;

Vector = rho_l/mP*(phip'*nor_ar_p)';

VVp = Up + rho_l/(mP)*(phip'*nor_ar_p)';


VVp(1);
Rpp = VVp;
vpp = zeros(size(vp));
vpp(:,1) = VVp(1);
vpp(:,2) = VVp(2);
vpp(:,3) = VVp(3);

qp = rho_l/mP * normalsp * (phip'*nor_ar_p)' + normalsp*Up;


vpp1(:,1)=qp.*normalsb(:,1)+wp(:,1);
vpp1(:,2)=qp.*normalsb(:,2)+wp(:,2);
vpp1(:,3)=qp.*normalsb(:,3)+wp(:,3);



Upp = (1 - rho_l/rhoP)*[0;0;g] + ...
   (rho_l/mP) * ( (1/2)*(dot(vpp1',vpp1')*nor_ar_p)' - (dot(nor_ar_p',vpp1')*vpp1)') ;
                                       


for i=1:Nbub
    k=(nodeBN*(i-1)+1:nodeBN*i)';
    phibp(k)=Filter*phibp(k);
    vbp(k,1)=Filter*vbp(k,1);
    vbp(k,2)=Filter*vbp(k,2);
    vbp(k,3)=Filter*vbp(k,3);
end;

