function [vnew,vmemnew,phinew,phimemnew]=P3DBL_ab4(time,v,vmem,phi,phimem)
%one step of the Adams-Bashforth 4th order 
global ht;
[vp,phip]=P3DBL_rhs(time,v,phi);
vmemnew=zeros(size(vmem));
phimemnew=zeros(size(phimem));
vnew=v+ht*(55*vp-59*vmem(:,:,1)+37*vmem(:,:,2)-9*vmem(:,:,3))/24;
phinew=phi+ht*(55*phip-59*phimem(:,1)+37*phimem(:,2)-9*phimem(:,3))/24;
vmemnew(:,:,3)=vmem(:,:,2); vmemnew(:,:,2)=vmem(:,:,1); vmemnew(:,:,1)=vp;
phimemnew(:,3)=phimem(:,2); phimemnew(:,2)=phimem(:,1); phimemnew(:,1)=phip;
