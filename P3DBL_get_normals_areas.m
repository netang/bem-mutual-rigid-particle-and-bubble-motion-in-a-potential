function [normals,areas]=P3DBL_get_normals_areas(v)
global faces;

Nvert=length(v);
Nface=length(faces);
areas=zeros(Nvert,1);
normals=zeros(Nvert,3);
v1=faces(:,1); v2=faces(:,2); v3=faces(:,3);
r1=v(v1,:); r2=v(v2,:); r3=v(v3,:);
vec1=r2-r1; vec2=r3-r1;
vec=.5*cross(vec1,vec2);
ar=sqrt(dot(vec',vec'))';
for j=1:Nface
    areas(v1(j))=areas(v1(j))+ar(j);
    areas(v2(j))=areas(v2(j))+ar(j);
    areas(v3(j))=areas(v3(j))+ar(j);
    normals(v1(j),:)=normals(v1(j),:)+vec(j,:);
    normals(v2(j),:)=normals(v2(j),:)+vec(j,:);
    normals(v3(j),:)=normals(v3(j),:)+vec(j,:);
end;

areas=areas/3;
dnor=sqrt(dot(normals',normals')');
normals(:,1)=normals(:,1)./dnor;
normals(:,2)=normals(:,2)./dnor;
normals(:,3)=normals(:,3)./dnor;
