function [vnew,vp,phinew,phip]=P3DBL_rk1(time,v,phi)
%one step of the Runge-Kutta 1th order with saving of the rhs at t=time
global ht vpn_init;
[vp,phip,vpn]=P3DBL_rhs(time,v,phi);
vnew=v+ht*vp;
phinew=phi+ht*phip;
vpn_init=vpn;