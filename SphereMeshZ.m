function [faces,vertices]=SphereMeshZ(MeshFreq,R)

r=2;
tau=(1+sqrt(5))/2;
vert1=[1,0,tau;-1,0,tau]; vert1=[vert1;-vert1];
vert2=[vert1(:,3), vert1(:,1), vert1(:,2)];
vert3=[vert1(:,2), vert1(:,3), vert1(:,1)];
vert=[vert1;vert2;vert3];
clear tau vert1 vert2 vert3;

x=vert(:,1);
y=vert(:,2);
z=vert(:,3)+r;
clear vert;

x_proj=2*r*x./(2*r-z);
y_proj=2*r*y./(2*r-z);
tri = delaunay(x_proj,y_proj);

z=z-r;
I=[x,y,z];

for i=1:size(x), countnum(i)=sum(sum(tri==i)); end;
unique(countnum);

tri=[tri;1 2 9; 1 2 12];
for i=1:size(tri,1)
    a=I(tri(i,1),:); b=I(tri(i,2),:); c=I(tri(i,3),:);
    norm=cross(b-a,c-a);
    direction=norm.*a;
    if direction <=0, tri(i,:)=[tri(i,1),tri(i,3),tri(i,2)]; end;
end

for j=1:MeshFreq

    new_tri=[];
    no_tri=size(tri,1);
    no_pnt=size(x,1);
    for i=1:no_tri
        new_tri=[new_tri;tri(i,1),no_pnt+i,no_tri*2+no_pnt+i;...
            no_pnt+i,tri(i,2),no_tri+no_pnt+i;...
            no_tri+no_pnt+i,tri(i,3),no_tri*2+no_pnt+i;...
            no_pnt+i,no_tri+no_pnt+i,no_tri*2+no_pnt+i];
    end

    II1=(I(tri(:,1),:)+I(tri(:,2),:))/2;
    l=sqrt(II1(:,1).*II1(:,1)+II1(:,2).*II1(:,2)+(II1(:,3)).*(II1(:,3)));
    IIx=r*II1(:,1)./l ; IIy=r*II1(:,2)./l ;IIz=r*II1(:,3)./l ;
    II1=[IIx,IIy,IIz];

    II2=(I(tri(:,2),:)+I(tri(:,3),:))/2;
    l=sqrt(II2(:,1).*II2(:,1)+II2(:,2).*II2(:,2)+(II2(:,3)).*(II2(:,3)));
    IIx=r*II2(:,1)./l ; IIy=r*II2(:,2)./l ;IIz=r*II2(:,3)./l ;
    II2=[IIx,IIy,IIz];

    II3=(I(tri(:,3),:)+I(tri(:,1),:))/2;
    l=sqrt(II3(:,1).*II3(:,1)+II3(:,2).*II3(:,2)+(II3(:,3)).*(II3(:,3)));
    IIx=r*II3(:,1)./l ; IIy=r*II3(:,2)./l ;IIz=r*II3(:,3)./l ;
    II3=[IIx,IIy,IIz];

    x=[x;II1(:,1);II2(:,1);II3(:,1)];
    y=[y;II1(:,2);II2(:,2);II3(:,2)];
    z=[z;II1(:,3);II2(:,3);II3(:,3)];

    %dist1=sqrt((II1(1,1)-II2(1,1))*(II1(1,1)-II2(1,1))+(II1(1,2)-II2(1,2)))*(II1(1,2)-II2(1,2))+(II1(1,3)-II2(1,3))*(II1(1,3)-II2(1,3))
    %dist2=sqrt((I(tri(1,1),1)-II1(1,1))^2+(I(tri(1,1),2)-II1(1,2))^2+...
    %             (I(tri(1,1),3)-II1(1,3))^2)

    clear II1 II2 II3 IIx IIy IIz l;

    [I,ndx1,ndx2] = unique([x y z],'rows');
    x=I(:,1); y=I(:,2); z=I(:,3);
    tri=new_tri;
    tri = reshape(ndx2(tri),size(tri));

end

faces=tri;
vertices=zeros(size(I)); dI=sqrt(dot(I',I')'); 
vertices(:,1)=R*I(:,1)./dI;
vertices(:,2)=R*I(:,2)./dI;
vertices(:,3)=R*I(:,3)./dI;







