% This script consider global variables

% Particle global variable
global mP rhoP Rp0 vp0

% Liquid global variable
global sigma rho_l mu a;

global faces;
global AllNeighbors AllNeighborFaces NeiBookmarks;

% time global variable
global ht tmax hh;

% bubble global variable
global kappa

% programm's flags
global odemem odenomem comp_u;

global Vg_mas time_mas Filter Rp_mas Up_mas Bc_mas;
global Pg0 P0 phog0 Vg0  mg0 T0 Rg omega Ap g;
global Bc nodeBN faceBN indv Nbub alpha S option vpn_init coef_ab_n;
mP=[]; Rp0=[]; vp0=[]; rhoP=[];
sigma=[]; mu=[]; a=[];
faces=[];
AllNeighbors=[]; AllNeighborFaces=[]; NeiBookmarks=[];
ht=[]; hh=[]; %tmax=[]; 
odemem=[]; odenomem=[]; comp_u=[];
Vg_mas=[]; time_mas=[]; Filter=[];
Pg0=[]; P0=[]; phog0=[]; Vg0=[]; gamma=[]; mg0=[]; T0=[]; 
Rg=[]; omega=[]; Ap=[];  g=[]; 
Bc=[]; nodeBN=[]; faceBN=[]; indv=[]; Nbub=[]; alpha=[]; S=[];
option=[]; vpn_init=[]; coef_ab_n=[]; Rp_mas=[]; Up_mas=[]; Bc_mas=[];